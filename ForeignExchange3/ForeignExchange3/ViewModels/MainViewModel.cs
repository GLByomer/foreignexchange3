﻿



namespace ForeignExchange3.ViewModels
{
    using System;
    using System.Collections.Generic;//<List<>>
    using System.Collections.ObjectModel; //ObservableCollection
    using System.ComponentModel;//INotifyPropertyChanged
    using System.Net.Http; //HttpClient
    using System.Windows.Input;//ICommand
    using GalaSoft.MvvmLight.Command; //RelayCommand
    using Models; // pour declarer table Rate dans dossier models
    using Newtonsoft.Json; //JsonConvert

    public class MainViewModel :INotifyPropertyChanged
    {
        #region Events
        public event PropertyChangedEventHandler PropertyChanged; //PropertyChanged
        #endregion

        #region Attributs
        bool _IsRunning;
        string _result;
        #endregion

        #region Properties
        public string Amount
        {
            get;
            set;
        }

        public ObservableCollection<Rate> Rates  //table Rate
        {
            get;
            set;
        }

        public Rate SourceRate   //Name Champ en tableRate
        {
            get;
            set;
        }

        public Rate TargetRate  //Name Champ en tableRate
        {
            get;
            set;
        }

        public bool IsRunning   //Name Champ en tableRate
        {
            get
            {
                return _IsRunning;
            }
            set
            {
                if (_IsRunning != value)
                {
                    _IsRunning = value;
                    PropertyChanged.Invoke(this, new PropertyChangedEventArgs(nameof(IsRunning)));
                }
            }
        }
        public bool IsEnabled   //Name Champ en tableRate
        {
            get;
            set;
        }

        public string Result  //Name Champ en tableRate
        {
            get
            {
                return _result;
            }
            set
            {
                if (_result != value)
                {
                    _result = value;
                    PropertyChanged.Invoke(this, new PropertyChangedEventArgs(nameof(Result)));
                }
            }
        }

        #endregion

        #region Constructors
        public MainViewModel()
        {
            LoadRates();

        }
        #endregion



        #region Methods
        async void LoadRates()
        {
            IsRunning = true;
            Result = "Loading Rates ...";

            try
            {
                var client = new HttpClient();
                client.BaseAddress = new Uri("https://apiexchangerates.azurewebsites.net ");
                var controller = "/api/rates";
                var  response = await client.GetAsync(controller);
                var result = await response.Content.ReadAsStringAsync();
                if (!response.IsSuccessStatusCode)
                {
                    IsRunning = false;
                    Result = result;
                }
                var rates = JsonConvert.DeserializeObject<List<Rate>>(result);
                Rates = new ObservableCollection<Rate>(rates);

                IsRunning = false;
                IsEnabled = true;
                Result = "Read to Convert!";

               // 12:39timvedio


            }
            catch(Exception ex)
            {
                IsRunning = false; 
                Result = ex.Message;
            }
        }
        #endregion



        #region Commands
        public ICommand ConvertCommand  //ICommand =using System.Windows.Input;         
        {
            get
            {
                return new RelayCommand(Convert); //// RelayCommand=a jouter packge mvvm light using GalaSoft.MvvmLight.Command;
            }
        }

        void Convert()
        {
            throw new NotImplementedException();
        }
        #endregion

    }
}
